
# Inkscape Manuals

This project is dedicated to the creation and to editing manuals for [Inkscape](https://inkscape.org).

Each manual team can opt to write their manual on a different platform, or to create a folder in this repository for their manual.

The team can make use of the [issues section](https://gitlab.com/inkscape/inkscape-docs/manuals/issues) to track their progress and their members' responsibilities.
Everyone can create new issues/tasks.
Issues for each manual need to be tagged with the name of the manual they belong to.

## Inkscape Beginners' Guide

[![Documentation Status](https://readthedocs.org/projects/inkscape-manuals/badge/?version=latest)](https://inkscape-manuals.readthedocs.io/en/latest/?badge=latest)

### What's the Goal of the Guide?

This book is meant as an introduction for people who are not yet familiar with Inkscape terminology, and don't want to hear long explanations about how something works 'under the hood', but rather just want to get started quickly.

It is - at least at its beginning - a translation of the book ['Initiation à Inkscape'](https://fr.flossmanuals.net/initiation-inkscape/_draft/_v/1.0/introduction/), written in French by Elisa de Castro Guerra.

### Where is the Book being Created?

#### Editing platform:

This book is created in Inkscape's Manual repository on [GitLab](https://gitlab.com/inkscape/inkscape-docs/manuals), which is a website that allows for collaborative editing and sharing of code and other text files. 

It is available for reading and downloading on [Read the Docs](https://inkscape-manuals.readthedocs.io/).

If you need help with using the GitLab platform, please ask one of the project's maintainers, or send an email message to [Inkscape's documentation mailing list](https://inkscape.org/en/community/mailing-lists/).

#### Task list and task-focused discussion:

Task progress is being tracked [here](https://gitlab.com/inkscape/inkscape-docs/manuals/issues), with each issue tagged "Beginners' Guide".

#### Discussions of global interest:

Major topics can be discussed on the [inkscape-docs mailing list](https://sourceforge.net/p/inkscape/mailman/inkscape-docs/), which is also where status updates will be posted from time to time. New members are encouraged to say hi there, and state what part of the book they would like to help with.

#### Discussions when editing the book:

Anyone who edits the book can exchange short messages, files and snippets via chat with other contributors on [Inkscape's chat platform](http://chat.inkscape.org/). We do not have a dedicated channel yet, so the #general channel is currently the best place. For important topics, please use one of the other two communication channels.

### How do I join?

To join the Beginners' Guide editors, just say 'Hi' on the mailing list, and coordinate with others which files you would like to work on.

To find a task that you would like to help with, look for open tasks in the [issue tracker](https://gitlab.com/inkscape/inkscape-docs/manuals/issues), and add a comment on the task if you would like to take it on, or tackle a part of it. Only take on a task when you know you will be able to work on it. When you can no longer help with a task, please unassign yourself and let the other editors know in a comment, so someone else can pick up where you left.

Subscribe to the [inkscape-docs mailing list](https://sourceforge.net/p/inkscape/mailman/inkscape-docs/) for updates and discussions revolving around the manual (and other Inkscape documentation). This is also the place where you can introduce yourself to the team and where you can ask questions when you need help, or just info (or where you can answer other people's questions).

### What do I need to know before I start editing away?

#### Large changes:

When you've got an idea that would introduce a major change in the book's structure, please confer with other editors on the mailing list to see what they think about this.

#### Writing Style:

When deciding how to phrase something, think of yourself when you started to use a vector editor, or think of a teen who you would like to introduce to the program, and who you would like to enjoy the process, and to be able to make nice drawings after reading the book.

Stay on topic, make it easy to understand, and keep the learning curve shallow. Write less rather than more. Be nice and encouraging to the reader. There exists another manual that provides the deep, technical details. We need to keep in mind that it will also be read by people whose native language is not English.

For consistent terminology, please refer to the [Inkscape Glossary](http://wiki.inkscape.org/wiki/index.php/Inkscape_glossary).

#### Inkscape Version:

This book currently refers to the Inkscape 0.92.x series (important for your screenshots and any description of functionality!).

### Under what Licence will the Book be Published?

This book is licensed under the [Creative Commons Attribution Share Alike 4.0 license](CC BY-SA 4.0, see https://creativecommons.org/licenses/by-sa/4.0/).
By contributing to this book, you agree to publish your content under this license.

As a contributor, please add your name to the list in the 'About this Book' chapter. This chapter will *always* be in need of new content ;-)

### Go!

Have fun working on the book at https://gitlab.com/inkscape/inkscape-docs/manuals/.
