*************************
Inkscape Beginners' Guide
*************************

This is the collaborative area for the Inkscape Beginners' Guide, an introductory practical manual for people who are new to Inkscape.

It is based on the book '`Initiation à Inkscape <https://fr.flossmanuals.net/initiation-inkscape/>`_' by Elisa de Castro Guerra.

To build it on your own computer, you need to have `Sphinx <www.sphinx-doc.org/>`_ installed (e.g. install the package "python3-sphinx" on Ubuntu). For being able to export to pdf, you also need `LaTeX <https://www.latex-project.org/>`_.

The book's contents is licensed CC BY-SA.

Building the Guide
==================

To build a set of html files (generated index file is ``build/html/index.html``), run:

.. code-block:: none

  make html

To build a pdf file (generated file is ``build/latex/InkscapeBeginnersGuide.pdf``), run:

.. code-block:: none

  make latexpdf

To remove the build directory before building, run

.. code-block:: none

  make clean

This can become necessary when you make changes in one file that also affect other html files, e.g. changing a title (that will change in the menu), adding a chapter (that will appear in the menu), or when you change static files (css, js, images), as Sphinx seems to only pick up changes in .rst files.

TODO
====

Content:

- rethink introductory chapters (titles sound wrong, need an explanation about what Inkscape is)
- consider adding intro chapters to each section (some have one)
- write / fix anything else that is marked with TODO in the source folder
- markup useful terms, add them to glossary and write definitions
- decide on 'we <are going to>...' vs. 'you <are going to>...' and make it consistent throughout
- add more subheadings

Images:

- change out all icons for the actual Inkscape icon SVGs
- create missing, untranslated screenshots
- find out if it is at all possible to influence image sizes in pdf export, via some kind of 'classes'
- when all is ready, remove unused images (not icons)

General layout:

- setup a generalized chapter head and make it look nice:

  * heading
  * icon / keyboard shortcut if applicable
  * how to get there via menu, if applicable
- find out how to add at least some minimal styling to pdf
- remove the old fonts / css directory after it was incorporated into pdf styling
- use non-breaking spaces in keyboard shortcut combinations
- make guilabel background color darker

Infrastructure:

- adjust branch after merge (on readthedocs), add draft branch, where people can test their changes before they are merged
- close book draft on flossmanuals, add info about new home
- ? Add CI to test for syntax errors ? into the css / pdf builder
- add sphinx_rtd_theme, sphinx, latex for local install (to always have the most current one and to not have to worry about licences, script this in a setup.py for local install)
- Add license for Inkscape GUI icons into icons directory

Super Bonus Extras:

- test js. If it works, then:

  * explore js to load glossary term definitions from the glossary page (same domain, good!), and show them on hover in a text bubble (or find someone who would like to write that)
  * explore js that scrolls menu to correct place (or find someone who would like to write that). It needs to scroll to an element in the navigation that has both the class 'current' and whose link is that of the current page, when the page is loaded (but preferably, it should not scroll visibly, but before the navigation appears). Must work both on mobile and desktop.
