******************
The User Interface
******************

Inkscape shows a window for each opened document. Each window contains
the toolbars, and a white, empty area.

In this manual, we use the :guilabel:`Wide` view which puts the first toolbar
on the right. You can get this view with the menu by selecting :menuselection:`View --> Wide`.

|The Inkscape Interface|

-  The application **menubar** along the top provides general menu
   options. Some are similar to other software programs (:menuselection:`File
   --> Save`, :menuselection:`Edit --> Copy`, etc.). There are also
   Inkscape-specific ones.
-  The **tool controls bar** just below adjusts to the currently selected
   tool. It displays the tool's options.
-  Vertically on the left, the **toolbox** contains the main drawing
   tools. Only one tool can be selected at once.
-  The large blank area is the **canvas**, where the image is edited. A
   black outline represents the visible page.
-  On the right side of the window, there are two toolbars. To the left
   is the **commands bar** which gives quick access to common commands,
   which are also available via the dropdown menus. If not all the
   commands are shown, there is a right-facing arrow that gives access
   to the hidden choices.
-  To the right is the **snap controls bar**. We suggest you deactivate
   snapping for now, by pressing the topmost button in the bar, or by
   pressing :kbd:`%`.
-  There are **rulers** at the top and on the left of the canvas to help
   with grid and guideline placement.
-  **Scrollbars** are available to the right and bottom to move around on
   the canvas.
-  The **color palette** toolbar is near the bottom of the window. Its
   most basic usage is to change the fill color of an object.
-  At the very bottom, the **status bar** provides information such as the
   colors of the selected object, layers, cursor coordinates, and zoom
   level. It also contains a field where Inkscape can display helpful
   texts, such as the number and type of selected objects, or tips about
   keyboard shortcuts and usage hints. Whenever Inkscape doesn't do what
   you think it should be doing, look here first.

Other options are available via popup dialog boxes which will appear
attached to the right of the canvas, in the dock area:

|Docked dialog|

.. |The Inkscape Interface| image:: images/interface_win_10.png
.. |Docked dialog| image:: images/docked_dialog_win10.png
