********
3D-Boxes
********

|Large Icon for 3D-Box Tool| :kbd:`Shift` + :kbd:`F4` or :kbd:`x`

The **3D-Box** tool draws a (simulated) three-dimensional box. To
understand how it works, it helps to know a little bit about drawing in
perspective. This tool is of interest to people who want to draw
architecture or interiors. It makes it really easy to create furniture
or rooms, because it automatically creates perspective, by making use of
vanishing points.

After click-and-dragging with the tool active, a box appears on the
canvas. This is best done somewhere near the center of the page area.
Else, the box can sometimes end up quite deformed. By default, two
vanishing points are placed in the vertical middle of the page borders,
one on the right, the other on the left.

When you look closely at the 3D-box that you have drawn, you will see
the following:

-  A small **black crosshair**, which designates the center of the box.
   When you click and drag it with your mouse, the box will move along
   with it, apparently in 3D space. Depending on its position in
   relation to the vanishing points, you will be able to see the box's
   bottom, or its top. If you move the box to the left or right, its
   shape will change in accordance with the vanishing lines.
-  Numerous (to be exact, 8) **white, diamond-shaped handles** allow you
   to extend or shrink the box along the x, y or z axis by dragging them
   with the mouse.
-  The **white, square-shaped handles** symbolize the vanishing points
   and can be moved in the same fashion as the other handles.

Note that these specific changes can only be achieved when you use the
3D-box tool. If you move the box with the Selection tool, both the box
and the vanishing points will move.

The tool controls for this tool allow you to set parameters for the 3
vanishing points for the x, y and z axis. The buttons with the icon depicting two
parallel lines |Icon for Parallel Perspective (3D-Box)| will make the vanishing
lines parallel and have an immediate effect on the box on the canvas.

.. figure:: images/3d_box_default.png
    :alt: The default 3D-Box
    :class: screenshot

    Dark blue: the top of the box. Medium blue: its left side. Light blue:
    its right side.

.. figure:: images/3d_box_moving.png
    :alt: Moving the 3D-Box
    :class: screenshot

    Moving the 3D-Box

.. figure:: images/3d_box_moving_vanishing_points.png
   :alt: Moving the vanishing points
   :class: screenshot

   Moving the vanishing points.

.. figure:: images/3d_box_adding_vanishing_point.png
   :alt: Additional vanishing point
   :class: screenshot

   Adding a third vanishing point.

.. |Large Icon for 3D-Box Tool| image:: images/icons/draw-cuboid.*
   :class: header-icon
.. |Icon for Parallel Perspective (3D-Box)| image:: images/icons/toggle_vp_y.*
   :class: inline
