**************************
When do you need Inkscape?
**************************

There are many image creation and manipulation software programs.
However there are two main types of images:

-  **Raster** or **bitmap** images such as images from digital cameras.
   They are made up of :term:`pixels <Pixel>`, and the more the images are
   manipulated, the more the quality declines. Zooming or shrinking can cause
   blurriness or pixelation. Raster images can contain millions of colors and look very realistic.
-  **Vector** images are created with vector graphics software. They are
   made up of mathematically defined paths, and are independent of the
   resolution–the image adapts to the available space without losing quality.

   This is why the same vector image can be used for different sizes of the final
   image presentation. For example, the same image can be used for a
   business card or a large poster with equal quality.

   They often have an artificial and clean look, if they are used for drawing realistically. Vector images consist of separate objects, each of which can be modified separately. Each object has its own style (color, patterns, etc.).

Depending on how the image is used, either raster or vector graphics may
suit better to your needs.

TODO: Add some examples for each type

.. image:: images/vector-format.png
   :class: deco center medium
