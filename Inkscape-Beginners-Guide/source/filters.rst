*******
Filters
*******

One of the menu entries is dedicated to **filters**. More precisely, all
of these are combinations of SVG filters. The possibilities of
combination are infinite, and the Inkscape developers put some
combination suggestions into the menu and tried to find a descriptive
name for each.

To apply a filter, you need to first select one or more objects, then
select the filter you want to use. The result will immediately be
available on the canvas.

|image0|

The Drop Shadow Filter
======================

This filter promises to automatically add a shadow to the selected object that
will be placed below the object. First, select one or more objects, then open
the filter's dialog with :menuselection:`Filters --> Shadows and Glows --> Drop
Shadow`.

The dialog allows you to define the amount of blur, the shadow's
offset, it's type, and its color. The larger the blur radius, the more distant
will the object seem from its shadow. The farther away the values for the x and
y offset are from 0, the more will it look as if the light comes from the side.

The dropdown for the shadow type allows you to choose between:

- :guilabel:`Outer`: normal shadow
- :guilabel:`Inner`: the object looks like it were a hole
- :guilabel:`Outer Cutout`: just the outer shadow, the object will be invisible
- :guilabel:`Inner Cutout`: just the inner shadow, without the object
- :guilabel:`Shadow Only`: the full shadow, without the object

In the :guilabel:`Color` tab, you can change the shadow's color and opacity (the
intensity of the shadow).

If you want to play with different values, check the little box labelled
:guilabel:`Live Preview`, then the result will be shown on the canvas. It will
only be applied to the object when you click on :guilabel:`Apply`.

|image1|

Editing Filters
===============

Inkscape offers a dialog for editing filters, which can be opened from the entry
:guilabel:`Filter Editor` at the bottom of the :guilabel:`Filters` menu.

When the dialog opens, and there is no filter applied to the currently selected
objects, it will be empty. When there is an object with a filter selected, you
will see the components of the object's current filter.

The basic principle of the dialog is simple, but understanding all its
ramifications is highly complex. One needs to have a deep knowledge in SVG
filters and matrix mathematics to be able to understand how to use the
parameters of every available filter to achieve a specific result.

Because of this, the easier option may be to just play with the filter settings
randomly, starting from one of the stock filters that is similar to the desired
result.

Here is a list of some interesting filters:

- :menuselection:`Distort --> Chalk and Sponge`: turns objects into an interesting
  explosion.
- :menuselection:`Image Paint and Draw --> Pencil`: converts the selected objects to a
  sketch.
- :menuselection:`Scatter`: Several filters that give different scatter effects.
- :menuselection:`Blurs`: Offers several blur variations.
- :menuselection:`Morphology`: Offers filters that have to do with the objects'
  contours.

It's up to you to explore this further. The result of most filters depends
heavily on the selected objects.

.. |image0| image:: images/menu-filtre.png
.. |image1| image:: images/ombre-portee-w.png
