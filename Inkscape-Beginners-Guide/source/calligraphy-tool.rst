********************
The Calligraphy Tool
********************

|Large Icon for Calligraphy Tool| :kbd:`Ctrl` + :kbd:`F6` or :kbd:`C`

What was a goose quill in antiquity, is Inkscape's calligraphy tool in
the digital world. Ideally, it should be used with a graphics tablet and
stylus, with one hand on the graphics tablet and the other one on the
keyboard, which moves the canvas, so one can write without interruption.

.. figure:: images/callygraphy_menu.png
   :alt: Calligraphy tool options
   :class: screenshot

   A varied set of options exists for simulating different brushes, in case
   the default brushes that come with Inkscape do not suffice.

Dip Pen
  Emulates a bevelled pen tip.

Marker
  Emulates a round and regular tip.

Brush
  Emulates a soft, thinning ellipse.

Wiggly
  Emulates a very jumpy round brush.

Splotchy
  Emulates a quill.

Tracing
  This allows you to emulate an engraving, by drawing more
  or less regular lines over a drawing that serves as a model.

The button to the right, :guilabel:`Add or edit calligraphic profile`, allows
you to save and load the settings made in the following options, under a
brush name of your choice.

Every brush is a result of the following parameters:

Width
  Here you can set the width of your brush.

|Pressure sensitivity icon| Pressure sensitivity
  This option is only useful when a graphics
  tablet is used for drawing.

|Trace lightness icon| Trace lightness
  Needs to be active when you want to use
  the Engraving feature.

Thinning
  Determines the width of the brush stroke at the start
  and end of the line.

Angle
  Determines the quill's angle.

Fixation
  Determines how much the angle will change when the draw
  direction changes.

Caps
  Determines the shape of the brush stroke's ends.

Tremor
  Adds a little random to the brush stroke.

Wiggle
  Adds the unexpected.

Mass
  Simulates the weight of the tool used, which has an impact
  on the stroke's shape.

.. figure:: images/calligraphy_tool_dip_pen.png
   :alt: Dip Pen setting of Calligraphy Tool
   :class: screenshot

   The ends of a line drawn with the :guilabel:`Dip Pen` setting are cusp.

.. figure:: images/calligraphy_tool_marker.png
   :alt: Marker setting of Calligraphy Tool
   :class: screenshot

   With the :guilabel:`Marker` setting, the paths look smooth and their ends are
   rounded.

.. figure:: images/calligraphy_tool_brush.png
   :alt: Brush setting of Calligraphy Tool
   :class: screenshot

   The setting :guilabel:`Brush` creates slightly rounded and uneven strokes.

.. figure:: images/calligraphy_tool_wiggly.png
   :alt: Wiggly setting of Calligraphy Tool
   :class: screenshot

   The :guilabel:`Wiggly` setting allows you to draw very organic shapes.

.. figure:: images/calligraphy_tool_splotchy.png
   :alt: Splotchy setting of Calligraphy Tool
   :class: screenshot

   When set to :guilabel:`Splotchy`, the result looks a bit like calligraphy.

.. figure:: images/calligraphy_tool_tracing.png
   :alt: Tracing setting of Calligraphy Tool
   :class: screenshot

   With the calligraphy tool setting :guilabel:`Tracing`, drawings that look
   like an old engraving can be created. The image below serves as a model.



.. |Large Icon for Calligraphy Tool| image:: images/icons/draw-calligraphic.*
   :class: header-icon
.. |Pressure sensitivity icon| image:: images/icons/draw-use-pressure.*
   :class: inline
.. |Trace lightness icon| image:: images/icons/draw-trace-background.*
   :class: inline
