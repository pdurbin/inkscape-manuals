**********************
Managing the workspace
**********************

Document Properties Dialog
==========================

By default, Inkscape creates documents in a size that fits your operating
system language (that is, in the US, you get a different default document
than in Germany, for example). To change the page size, click
:menuselection:`File --> Document properties…` (or click on the next to last
icon on the commands bar, which looks like |image0|, or use the :kbd:`Shift` + :kbd:`Ctrl` + :kbd:`D` keyboard shortcut). In the section :guilabel:`Page Size` on the :guilabel:`Page` tab, select or define the size you wish.

|Document Properties Dialog|

The default measurement unit for page size can be changed in the :guilabel:`Page Size` section of the same :guilabel:`Page` tab.

The large rectangular line that you see on the canvas, in a new blank
document, is the page border. If you compare the page border to the
rulers, you can see that it represents the page size. Note that the page
border is never seen in exported or saved files.

Most of the time, everything that you want to show up in your finished
drawing, must be inside this border. Elements placed outside of the page
border usually can't be seen when you share your drawing, and can be
thought of as drafts, experiments or available resources.

If you do not want to see the page border while you're working, you can
uncheck the option :guilabel:`Show page border` in the :guilabel:`Page` tab
to hide it. You can also configure the page shadow and color of that shadow,
in that section, if you like.

In Inkscape, the canvas appears white. Even though it does not use the
traditional checkerboard pattern to indicate transparency (by default),
it really is transparent. If you'd like to see a checkerboard pattern
for transparency, please check the :guilabel:`Checkerboard background` option
in the section :guilabel:`Background` on the Page tab.

When a drawing is exported to a :term:`PNG` format, areas on the page where
there is no object will be transparent. You can change this by putting
an opaque object behind your drawing (usually a rectangle, the same size
as the page).

Another way to change this is by clicking on the color bar in the
:guilabel:`Background` section of the :guilabel:`Page` tab, and setting the
:guilabel:`A` slider ("A" for :term:`Alpha`) to 255, or all the way to the
right. You can also change the color, if you like. Note that this setting
only affects how the image looks in Inkscape or when you export to PNG.

|Dialog for setting the background color|

Options in 'View' Menu
======================

Many people use Inkscape with no scrollbars and no snapping. Any
unneeded bars can be hidden in the menu :menuselection:`View --> Show/hide`. The canvas can be moved using the middle mouse button.

|image3|

.. |image0| image:: images/doc-prop.png
.. |Document Properties Dialog| image:: images/document_properties_dialog_win10_1.png
.. |Dialog for setting the background color| image:: images/background_color_dialog_win10.png
.. |image3| image:: images/show_hide_win10.png
