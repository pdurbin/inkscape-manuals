***************************
Ways of Drawing in Inkscape
***************************

Inkscape offers several ways for creating vector images, which can, of course, be combined:

- using the geometric shape tools
- using the path tools, much like a pencil on paper
- starting from a photo, a scanned image, or any raster graphic by using a tracing engine
- using one of the many available features that let you create elements of a drawing automatically

Often there are more than one, or even several different ways to accomplish the same result. Soon you will develop your own habits and preferences for drawing with Inkscape. Only very rarely, there is one way, and **only** one way, to accomplish something.

In this section, we'll start out by exploring the easiest way to create a drawing in Inkscape: the shape tools. We will also get to know some of the most commonly used tools or features. If you have any prior experience with raster graphics (such as editing photos), you will also begin to discover how creating and editing vector graphics is so very different.
