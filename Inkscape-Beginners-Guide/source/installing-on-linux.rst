********************************
Installing Inkscape on GNU/Linux
********************************

There are many different ways to get Inkscape for your GNU/Linux operating system. Read on to find the best option for yourself.

Preinstalled
============
On many Linux distributions, Inkscape comes preinstalled—just select it from the "Graphics" section of your application menu to start it up.

From your distro's app store
============================

Many Linux distributions come with an app store. In yours, type "Inkscape" into the search field and select to install it. Note that the version you get from the store isn't necessarily the most recent one, and in some cases, also lacks some features.

The latest version directly from the Inkscape project
=====================================================

For downloading and installing the latest version, visit `the Download page on the Inkscape Website <https://inkscape.org/release>`__ and select one of the various options for GNU/Linux.

Depending on the Inkscape version and your Linux distribution, these installation formats may be available to you:

    * :term:`AppImage`
    * :term:`Flatpak`
    * :term:`ppa <Personal Package Archive>` (for Ubuntu and Ubuntu derivatives)
    * :term:`Snap`
    * building Inkscape yourself from the source code
