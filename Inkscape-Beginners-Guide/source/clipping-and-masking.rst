********************
Clipping and Masking
********************

These two features are both employed in the same way: Put the object that will
serve as clip (or mask) above the object that you want to clip (or mask). Then
select both objects, and use :menuselection:`Object -->Clip --> Set` or
:menuselection:`Object --> Mask --> Set`. The top object will then become
invisible and work to hide parts of the bottom object.

Clips and masks give different results. A clip allows you to hide all parts of
the bottom object that are outside the clip object. The colors of a mask object
affect the opacity of the bottom object. The darker and the more transparent the mask, the more transparent will the masked object become. A white mask works just like a clip.

.. figure:: images/clip_mask_1.png
   :alt: Original object
   :class: screenshot

   The object we want to clip and mask

.. figure:: images/clip_mask_2.png
   :alt: Both paths
   :class: screenshot

   The path for clipping (and masking) has been positioned above the head of the griffin.

.. figure:: images/clip_mask_3.png
   :alt: Result of clipping and masking
   :class: screenshot

   On the left, the griffin's head was clipped with the circle. On the right, the circle was used as a mask.
