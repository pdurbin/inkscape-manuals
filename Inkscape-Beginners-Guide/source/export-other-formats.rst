*******************************
Exporting to other File Formats
*******************************

In addition to :term:`PDF` and :term:`PNG`, Inkscape can export to a variety
of other file formats. A list of available formats in your own Inkscape
installation can be found at the bottom of the :menuselection:`File -->
Save`, :menuselection:`File --> Save as` and :menuselection:`File --> Save a
Copy` dialogs.

Some formats only become available when a certain other program is installed
on the computer. Third-party Inkscape extensions that you install yourself
can also add new file formats to the list.

No matter which format you want to export to, it is recommended to only use
the :guilabel:`Save a Copy` functionality for this, and to save your original
file as :guilabel:`Inkscape SVG`. This way, you can always come back and make
edits to your original drawing, and you will not lose helpful editor data,
such as guides, grids, path effects, layers and more.

For some of these file formats, you will get a dialog prompting
you to select further options for export.

Note that most export file formats do not support all Inkscape features.

Commonly available export file formats
======================================

*(ordered by file extension)*

Desktop Cutting Plotter (AutoCAD DXF R 12) (\*.dxf)
  plotter file format

Desktop Cutting Plotter (AutoCAD DXF R 14) (\*.dxf)
  plotter file format

Enhanced Metafile (\*.emf)
  improved version of .wmf with support for Bézier curves**

Encapsulated PostScript (\*.eps)
  embeddable printer graphics file

JavaFX (\*.fx)
  vector image files for use in JavaFX user interface

Flash XML Graphics (\*.fxg)
  XML-based file format (similar to SVG) for use with Adobe Flash and Adobe Flash Player

GIMP Palette (\*.gpl)
  color palette containing the colors used in the current document that can
  be used by several open graphics programs, such as Gimp, Inkscape or Krita

HTML 5 canvas (\*.html)
  HTML5 canvas object

OpenDocument drawing (\*.odg)
  open diagram file format, for use e.g. with LibreOffice

Optimised SVG (\*.svg)
  SVG file optimized for size

Portable Document Format (\*.pdf)
  standardized document exchange file format

HP Graphics Language plot file [AutoCAD] (\*.plt)
  plotter file format

Cairo PNG (\*.png)
  PNG file format that only supports a restricted set of PNG features. If
  you don't know what it is, you probably want :menuselection:`File -->
  Export PNG image` instead.

PovRay (\*.pov) (paths and shapes only)
  file format for a 3D ray tracing software

PostScript (\*.ps)
  printer graphics language file format

Synfig Animation (\*.sif)
  Synfig animation canvas file format

Plain SVG (\*.svg)
  SVG file that does not contain any editor data

Compressed Inkscape SVG (\*.svgz)
  gzip-compressed SVG file with all editor data

Compressed plain SVG (\*.svgz)
  gzip-compressed SVG file without editor data

Layers as Separate SVG (\*.tar)
  each layer of the current SVG file saved as a separate SVG, bound together
  in a tar archive

LaTeX With PSTricks macros (\*.tex)
  file format with support for complex math formulas commonly used for
  generating and type-setting large scientific documents, like doctoral
  theses

Windows Metafile (\*.wmf)
  proprietary Windows vector file format that only supports straight lines

Microsoft XAML (\*.xaml)
  Microsoft's graphical user interface description language file format

GIMP XCF maintaining layers (\*.xcf)
  Gimp's native file format, with Inkscape layers being converted to Gimp
  layers

Jessylnk zipped pdf or png output (\*.zip)
  JessyInk slide show with one pdf or png file per slide, compressed into a
  zip file

Compressed Inkscape SVG With media (\*.ZIP)
  Inkscape SVG file and linked image files, compressed into a zip file
