********
Glossary
********

.. glossary::

  Alpha
    ...

  AppImage
    ...

  Binary Archive
    is a folder that contains multiple binary files and requires a special type of program to interact with them.

  Binary File
    Binary files are files not meant to be read by humans. They contain no text but instead are composed of patterns 0s and 1s that can be directly read by the computer.

  Bit
    (short for binary digit) is the smallest unit of data in a computer. A bit has a single binary value, either 0 or 1.

  Cap
    ...

  Clone
    ...

  CSS
    ...

  Executable File (.exe)
    contains an executable program for Windows and is the standard file extension used by Windows programs. These are the files that make a program on your computer run.

  Flatpak
    ...

  Flow Text
    ...

  Glyph
    ...

  Hex Color
    ...

  HTML5
    ...

  Join
    ...

  Ligature
    ...

  Node
    A node is a point, placed at a specified position on a :term:`path <Path>`.

  Operating System Architecture
    A 32-bit architecture means that the operating system is able to process data chunks up to 32 bits in size. Likewise, a 64-bit architecture means an operating system can process data up to 64 bits. You need to use the correct program file for each architecture, otherwise the program won’t work properly.

  Path
    The basic building element in SVG, consists of :term:`nodes <Node>` that are connected by the segments of the path.

  PDF
    ...

  Pixel
    ...

  PNG
    ...

  Portable App
    A portable app is a program that does not need to be installed onto a computer. Instead all necessary files needed to run the program reside in a single folder located on a disc or USB drive. The advantage to this is that the program can be taken anywhere the user goes and can be run from the cd or USB drive on any computer that supports the program. The disadvantages are that the program can easily be lost and if the USB isn’t properly removed from the computer after use, the program can get corrupted and stop working.

  Personal Package Archive
    ...

  Snap
    ...

  Stroke
    ...

  Scalable Vector Graphics (SVG)
    ...

  SVG Font
    ...

  Windows Installer Package Files (.msi)
    An MSI file is a package that contains installation information for a particular installer, such as files to be installed and the locations for installation. They can be used for Windows updates as well as third-party software installers. The installation results are no different from using an executable file, but msi packages sometimes do have additional options such as doing silent installations (no human action needed) or pre-configured installs.

    Note: Most modern computers come as 64-bit systems.

  Zip File (.zip)
    Zip files are a way to compress large amounts of data into a smaller, more manageable size. This allows for easy transportation and faster downloads of files. The disadvantage to zip files is that the files inside need to be extracted and decompressed before they can be used.
