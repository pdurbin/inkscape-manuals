************
Styling Text
************

TODO: insert icon, :kbd:`Ctrl` + :kbd:`Alt` + :kbd:`T`, :menuselection:`Text --> Text and Font`

The :guilabel:`Text and Font` dialog is ideal for modifying a text's style. Most of the options that are available in that dialog can also be found in the horizontal tool controls bar at the top of the canvas:

TODO: list options for dialog and toolbar separately, this mixup is confusing. Adjust to 0.92.3 changes (how to explain those messed up line height settings???)

-  a field with a list of the fonts that are installed on your
   computer
-  the font style (bold, italic, condensed, regular etc.)
-  the font size
-  the line-height or spacing between lines
-  the icons for the text alignment (left, right, centered)
-  two icons for quickly adding superscript or subscript
-  a field for setting the spacing between letters and one for the
   spacing between words
-  two fields for horizontally and vertically shifting single or
   selected letters (kerning) and one for rotating them
-  icons for changing the text's direction and orientation.

In the dialog, you will see a small preview of the result of your
settings, and a tab labelled :guilabel:`Text` that can hold the whole text without
displaying any styling. Once you have all parameters tweaked to your
liking, you can click on :guilabel:`Apply` to make the result show up on the
canvas.

Inkscape does not come with any fonts. It will allow you to select from
those that you have installed on your system. When you install a new
font while you are working on a drawing, you need to close all Inkscape
windows and restart the program to be able to select the new font.

The tab :guilabel:`Variants` allows you to activate specific features of certain
fonts, like :term:`ligatures <Ligature>`.

There are several options that influence the text's orientation. Inkscape
offers support for vertical scripts, and also for text that is written from right to left, like Arabic or Hebrew. If your screen isn't wide enough to display the whole list of option buttons for the Text tool, you will see a little arrow on the far right of the tool controls bar. Click on it to reveal the missing options.

|image0|

*If I create a frame where the text will*

The text tool's options

*I write my text directly on the canvas*

*I hit Enter when I need to break a line*

The Text and Font dialog

.. |image0| image:: images/textes.png
