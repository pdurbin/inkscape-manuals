***************
Cloning Objects
***************

|Large create clone icon| :kbd:`Alt` + :kbd:`D` :menuselection:`Edit --> Clone --> Create Clone`

You can save yourself some time when you know how to use :term:`clones <Clone>`.
With :kbd:`Alt` + :kbd:`D`, a clone can be created just as quickly as a copy or a
duplicate. One only needs to remember that the option exists.

The new clone will appear right on top of the original, and will follow all
modifications made on the original, no matter if they affect its style (stroke,
fill,...) or its shape.

The clone's shape cannot be edited directly. Its colors can only be changed if the original has no color itself. Its size and rotation can be changed freely.

.. Tip::
   To remove a color from an object, you must 'unset' its paint, using the
   question mark button |Unset color icon| in the :guilabel:`Fill and Stroke`
   dialog.

.. figure:: images/clones.png
   :alt: The left, selected object is a clone of the right object.
   :class: screenshot

   All these stars are clones of the star in the top left corner. They have the same color and shape, but different sizes and rotations. Note that the selected clone does not have any star tool handles.

To turn a clone into an independent, fully editable object, you need to
**unlink** it from its original with :kbd:`Shift` + :kbd:`Alt` + :kbd:`D`, :menuselection:`Edit --> Clone --> Unlink Clone` or by clicking on the corresponding icon |Unlink clone icon| in the command bar.



.. |Large create clone icon| image:: images/icons/edit-clone.*
   :class: header-icon
.. |Unset color icon| image:: images/icons/paint-unknown.*
   :class: inline
.. |Unlink clone icon| image:: images/icons/edit-clone-unlink.*
   :class: inline
