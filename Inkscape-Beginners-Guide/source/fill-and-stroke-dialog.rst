**************************
The Fill And Stroke Dialog
**************************

This dialog can be opened in various ways:

-  menu :menuselection:`Object --> Fill and Stroke`
-  keyboard shortcut :kbd:`Shift` + :kbd:`Ctrl` + :kbd:`F`
-  via its icon |Icon for Fill and Stroke dialog|
-  or by double-clicking on the fields for fill and stroke at the bottom
   left of the Inkscape window.

The dialog has 3 tabs: one for the :guilabel:`fill`, one for the :guilabel:`stroke paint` and
one for the :guilabel:`stroke style`. The first row of buttons in the tabs for fill and stroke paint are for setting the type of fill or stroke paint that
you want to use:

- |Icon for 'no paint' in Fill and Stroke dialog| The **cross** needs
  to be active when you do not want to have any fill or stroke paint
  respectively.
- |Icon for 'solid paint' in Fill and Stroke dialog| The **first
  square** must be activated if you want to apply a flat, uniform
  color.
- |Icon for linear gradients in Fill and Stroke dialog| The **second
  square** with the gradually changing color must be set when you want
  to use a linear gradient.
- |Icon for radial gradients in Fill and Stroke dialog| The **third
  square** with the brighter color in the middle is for setting a
  radial gradient.
- |Icon for mesh gradients in Fill and Stroke dialog| The **next
  square** with its four quarters is needed for using a mesh gradient.
- |image6| The **patterned square** when active will allow you to apply
  a pattern.
- |Icon for swatches in Fill and Stroke dialog| The **white square** is
  useful when you want to reuse the fill for other objects in the same
  document. It acts like a library.
- |image8| The **question mark** does not set any specific color. The
  object inherits the color of its parent.

Below, you can customize your color - we will return to that in the next
chapter. At the bottom of all 3 tabs, there are two fields that set
values for the whole object:

- :guilabel:`Blur`: when you click, or click-and-drag in this field, you can
  apply a blur to the whole selected object. Be warned: in most cases,
  values greater than 2% are pretty useless.
- :guilabel:`Opacity`: when you click, or click-and-drag in this field, this
  will change the global opacity of the selected object - changing the
  opacity of fill and stroke equally.

|The Fill tab of the Fill and Stroke dialog|

The first tab of the :guilabel:`Fill and Stroke` dialog, when an object with a
plain light blue fill is selected.

|image10| |image11|

The second and third tab of the :guilabel:`Fill and Stroke` dialog. The stroke is
dark blue, and 10 mm wide.

.. |Icon for Fill and Stroke dialog| image:: images/fill_and_stroke_icon.png
   :width: 50px
   :height: 50px
.. |Icon for 'no paint' in Fill and Stroke dialog| image:: images/paint_none_icon.png
   :width: 50px
   :height: 50px
.. |Icon for 'solid paint' in Fill and Stroke dialog| image:: images/paint_solid_icon.png
   :width: 50px
   :height: 50px
.. |Icon for linear gradients in Fill and Stroke dialog| image:: images/paint_gradient_linear_icon.png
   :width: 50px
   :height: 50px
.. |Icon for radial gradients in Fill and Stroke dialog| image:: images/paint_gradient_radial_icon.png
   :width: 50px
.. |Icon for mesh gradients in Fill and Stroke dialog| image:: images/icons/paint-gradient-mesh.*
   :class: inline
.. |image6| image:: images/paint_pattern_icon.png
   :width: 50px
   :height: 50px
.. |Icon for swatches in Fill and Stroke dialog| image:: images/paint_swatch_icon.png
   :width: 50px
   :height: 50px
.. |image8| image:: images/paint_unset_icon.png
   :width: 45px
   :height: 47px
.. |The Fill tab of the Fill and Stroke dialog| image:: images/fill_and_stroke_dialog-fill_tab.png
   :width: 600px
.. |image10| image:: images/fill_and_stroke_dialog-stroke_tab.png
   :width: 300px
.. |image11| image:: images/fill_and_stroke_dialog-stroke_style_tab.png
   :width: 300px
