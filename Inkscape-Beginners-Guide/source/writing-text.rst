************
Writing Text
************

|image0| :kbd:`F8` or :kbd:`T`

Once the Text tool is active, you will have two options at your hands
about how to create a text.

When you want to add a text that consists only of a single word or a short expression, the easiest way to add it is to:

#. Left-click on the canvas to place the cursor at the desired position.
#. Type the text directly afterwards. The text will all be put into a
   single line, unless you hit :kbd:`Enter` to continue in the next line.

When you want to put your (longer) text into a specific area
that you have reserved for it:

#. Click-and-drag on the canvas. This will create a frame for a flowed
   text with a blue border, in which the text can be inserted.
#. Type on the keyboard. The text will appear in its frame and will not
   leave the frame area. When it reaches the border of the frame, it
   will automatically flow into the next line. The border of the text
   frame will turn red when the text doesn't completely fit into it.
#. You can change the dimensions of the text box by dragging on the
   white handle in the bottom right corner.

.. Note:: The text will not stretch or grow to fill the allotted space at all
  costs. It is up to you to adapt the size of the area or the font to your
  needs.

|image1|

*I write my text directly on the canvas*

A left-click on the canvas with the text tool positions the cursor.

*I write my text directly on the canvas*

*I hit Enter when I need to break a line*

This mode does not automatically break lines in the text.

*Or I create a frame where*

You need to create a text frame with the text tool first, if you want to
use a flowed text with automatic line breaks.

*Or I create a frame where*

*the text breaks*

At any time, you can modify the text frame using its handle. [TODO: the image
shows a bug - a text frame is only supposed to have a handle at the
bottom right, and no rectangle corner rounding handle. This is fixed in
trunk.]

TODO: add the other images (the text in italics should be in the screenshots)

.. |image0| image:: images/outil-texte.png
.. |image1| image:: images/illu-texte1.png
