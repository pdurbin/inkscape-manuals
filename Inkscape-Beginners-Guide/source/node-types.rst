****************
About Node Types
****************

|Large Icon for Node Tool| :kbd:`F2` or :kbd:`N`

You have probably noticed that the node handles don't all look the same. Inkscape has a number of different node types, that each behave in a specific way:

|Cusp Node Icon| Cusp nodes
  Used for creating corners, or for being able to freely modify the curvature of
  the path. The handles can be moved independantly. A cusp node is
  diamond-shaped.

|Smooth node icon| Smooth nodes
  Used for drawing beautiful, flowing curves. Both handles and the node are
  aligned on a straight line. Smooth nodes look like ordinary squares.

|Symmetric node icon| Symmetric nodes
  Used for drawing soft curves. The handles are not only on the same line, but
  also both at the same distance from the node. Symmetric nodes look like squares, too, but their handles always move together.

|Auto-smooth node icon| Auto-smooth nodes
  Used for drawing nice
  curves, without worrying about handles or segment shapes. The handles
  sit on a straight line, and their distance from the node adapts
  automatically when you move the node, so a smooth curve is drawn. A smooth node has a circular shape.

To change a node's type:

#. Switch to the **Node tool** by clicking on its icon.
#. Click on the path you want to modify.
#. Click on the node that you would like to convert to a different
   node type, or select multiple nodes.
#. Then click on the corresponding icon in the tool controls bar to
   set the node type.

Beginners often prefer to use cusp nodes, because they are easy to use,
although very often, smooth nodes would be the better choice.

.. figure:: images/nodes_cusp_example.png
   :alt: Cusp nodes make sharp corners.
   :class: screenshot

   Cusp nodes make sharp corners.

.. figure:: images/nodes_smooth_example.png
   :alt: A smooth node creates a rounded curve.
   :class: screenshot

   A smooth node creates a rounded curve.

.. figure:: images/nodes_symmetric_example.png
   :alt: A symmetric node creates a symmetrical curve.
   :class: screenshot

   A symmetric node creates a symmetrical curve.

.. figure:: images/nodes_auto-smooth_example.png
   :alt: Auto-smooth nodes adapt automatically when you move them.
   :class: screenshot

   Auto-smooth nodes adapt automatically when you move them.



.. |Large Icon for Node Tool| image:: images/icons/tool-node-editor.*
   :class: header-icon
.. |Cusp Node Icon| image:: images/icons/node-type-cusp.*
   :class: inline
.. |Smooth node icon| image:: images/icons/node-type-smooth.*
   :class: inline
.. |Symmetric node icon| image:: images/icons/node-type-symmetric.*
   :class: inline
.. |Auto-smooth node icon| image:: images/icons/node-type-auto-smooth.*
   :class: inline
