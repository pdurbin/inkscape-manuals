************************
Creating a Mesh Gradient
************************

|Large Mesh Gradient icon|

This tool is especially useful for those who want to create photorealistic
designs, but it also has its uses for everyone who wants to create complex
gradients in a single object.

To apply a mesh gradient to an object, select the object.

Now you have two options:

Option 1:
  Activate the Mesh Gradient tool in the tool bar. Use it to click and drag on
  the object.

Option 2:
  Open the :guilabel:`Fill and Stroke` dialog. There, select the **Mesh
  Gradient** mode |Mesh Gradient Paint icon|.

Now, the mesh gradient will be displayed directly on the object. It has some
different kinds of nodes:

- grey diamond-shaped nodes for assigning colors
- white circular (or arrow-shaped) handles for shaping the mesh

.. Note::
   You will only be able to see and modify these nodes when you use the Mesh
   Gradient tool.

In the tool's tool controls bar, note the fields labelled :guilabel:`Rows` and
:guilabel:`Columns`. More rows or columns add more nodes that can each have a
separate color. This way, an object can be painted in a multitude of
colors. New meshes will have the set number of rows or columns.

To add more rows or columns to an existing gradient, double-click on the
vertical or horizontal mesh lines.

To apply a color: 

#. select a grey node
#. select the color of your choice.

.. Tip::
  You can use the dropper icon |Color Picker icon| at the bottom of the :guilabel:`Fill and Stroke` dialog to more quickly apply colors that you already use in your drawing.

Just like the normal gradients, **mesh gradients** can be shared between
objects, when both objects use a gradient with the same name (e.g.
mygradient1234). Select the gradient in the :guilabel:`Mesh Fill` list in
the :guilabel:`Fill and Stroke` dialog to reuse it on a different object.

.. figure:: images/mesh_gradient_cloud.png
   :alt: Cloud with mesh gradient
   :class: screenshot

   This cloud consists of 10 different colors. Each color has been applied
   to one of the grey diamond-shaped nodes. The shape of the mesh has been
   modified to better fit the shape of the cloud.

.. figure:: images/mesh_gradient_landscape_with_mesh.png
   :alt: Landscape with mesh gradients
   :class: screenshot

   A landscape with mesh gradients

.. figure:: images/mesh_gradient_landscape_without_mesh.png
   :alt: Same landscape without mesh gradients
   :class: screenshot

   The same landscape without mesh gradients



.. |Large Mesh Gradient icon| image:: images/icons/mesh-gradient.*
   :class: header-icon
.. |Mesh Gradient Paint icon| image:: images/icons/paint-gradient-mesh.*
   :class: inline
.. |Color Picker icon| image:: images/icons/color-picker.*
   :class: inline
